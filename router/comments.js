var express = require('express');
var router = express.Router();
var formidable = require('formidable');
var path = require('path')
var con = require(path.join('../db.js'));


router.get('/:page/:count/:eventid', async function(req, res) {
    var eventid = con.escape(req.params.eventid);
    var userid = req.user.id;
    var count = req.params.count;
    var page = req.params.page;
    await con.query(`SELECT comment.*,(SELECT COUNT(*) FROM comments_user_like WHERE comments_user_like.Comment_Id = comment.Id) AS Count,user.Profile AS Profile,IFNULL(comments_user_like.Status,0) as Status,user.Verify FROM comment LEFT JOIN comments_user_like ON comments_user_like.User_Id = ${userid} AND comments_user_like.Comment_Id = comment.Id  INNER JOIN user ON user.Id = comment.User_Id WHERE comment.Event_Id = ${eventid} ORDER BY comment.Date DESC LIMIT ${con.escape(count * page)}, ${count};SELECT IF(COUNT(*) > ${count}, COUNT(*) - ${count}, 0) as Total FROM comment INNER JOIN user ON user.Id = comment.User_Id WHERE comment.Event_Id = ${eventid}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result[0].concat(result[1]));
    });
});


router.post('/like', async function(req, res) {
    await con.query("INSERT INTO comments_user_like (User_Id, Comment_Id, Status) VALUES (" + con.escape(req.user.id) + ", " + con.escape(req.body.Comment_Id) + " , 1)", function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "liked"
        }));
    });
});

router.post('/unlike', async function(req, res) {
    await con.query("DELETE FROM comments_user_like WHERE comments_user_like.User_Id = " + con.escape(req.user.id) + " AND comments_user_like.Comment_Id = " + con.escape(req.body.Comment_Id), function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "unliked"
        }));
    });
});

router.post('/create', async function(req, res) {
    var comment = req.body;
    await con.query(`INSERT INTO comment (Username,Comment,Event_Id,User_Id)
                    VALUES(${con.escape(comment.Username)},${con.escape(comment.Comment)},${con.escape(comment.Event_Id)},${con.escape(req.user.id)})
    `, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        var query = `INSERT INTO notification (Title,Hashtag,Type,Picture,User_Id_from,User_Id_to,Action_Id)
        SELECT 'commented:',${con.escape(comment.Comment)},5,' ',${con.escape(req.user.id)},user.Id,${comment.Event_Id} FROM friend,user,event_user WHERE 
        (user_one_id = ${con.escape(req.user.id)} AND user.Id = friend.user_two_id OR user_two_id = ${con.escape(req.user.id)}
             AND user.Id = friend.user_one_id) AND status = 1 AND event_user.User_Id = user.Id AND event_user.Event_Id = ${comment.Event_Id}`;
        con.query(query, function(err, result, fields) {
            if (err) res.status(400).send(err.message);
        });
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Sent comment"
        }));
    });
});

module.exports = router;