var express = require('express');
var router = express.Router();
var formidable = require('formidable');
var path = require('path')
var con = require(path.join('../db.js'));

router.get('/:id', async function(req, res) {
    let id = req.params.id;
    await con.query("SELECT *, '' as Password FROM user WHERE Id = ?", [id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result[0]);
    });
});

router.get('/events/:id', async function(req, res) {
    let id = con.escape(req.params.id == 0 ? req.user.id : req.params.id);
    await con.query(`SELECT * FROM 
    (SELECT event.Id AS Event_Id,event.Picture,event.Date FROM event,event_user WHERE event.Id = event_user.Event_Id AND event_user.User_Id = ${id}
    UNION ALL
    SELECT event.Id AS Event_Id,event.Picture,event.Date FROM event WHERE event.Created_Id = ${id}) as row
    ORDER BY row.Date DESC`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});

router.post('/changepassword', async function(req, res) {
    let password = con.escape(req.body.pass);
    let id = con.escape(req.user.id);

    await con.query(`UPDATE user SET user.Password = ${password} WHERE user.Id = ${id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Updated User!"
        }));
    });
});

router.post('/update', async function(req, res) {
    let user = req.body;
    user.Id = req.user.id;
    await con.query("UPDATE user SET Firstname = ?, Lastname = ?, Username = ?, Email = ?,  About = ?, Address = ?, Profile = ?, Facebook = ?, Twitter = ?, Pinterest = ? WHERE user.Id = ?", [user.Firstname, user.Lastname, user.Username, user.Email, user.About, user.Address, user.Profile, user.Facebook, user.Twitter, user.Pinterest, user.Id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Updated User!"
        }));
    });
});

router.get('/notifications/:count', async function(req, res) {
    var id = req.user.id;
    var count = req.params.count;
    await con.query("SELECT notification.*,user.Verify,user.Username,user.Profile as U_Picture FROM notification INNER JOIN user ON user.Id = notification.User_Id_from WHERE notification.User_Id_to = ? ORDER BY notification.Date DESC LIMIT ?, 25;", [id, count * 25], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});

router.post('/profile/:name', function(req, res) {
    res.status(200).sendFile(__dirname + "/uploads/profile/" + req.params.name);
});


module.exports = router;