var express = require('express');
var router = express.Router();
var path = require('path')
var con = require(path.join('../db.js'));

router.get('/db', function(req, res) {
    if (con.state === 'disconnected') {
        res.send("DB: Server is down");
    }
    res.send("DB: Working");
});

router.get('/server', function(req, res) {
    res.send("Server: Working");
});
module.exports = router;