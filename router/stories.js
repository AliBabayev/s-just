var express = require('express');
var router = express.Router();
var path = require('path')
var formidable = require('formidable');
var con = require(path.join('../db.js'));

router.get('/:event_id/friends/', async function(req, res) {
    let user_id = con.escape(req.user.id);
    let event_id = con.escape(req.params.event_id);
    await con.query(`SELECT * FROM (SELECT user.Id, user.Username, user.Profile, event_stories.Stories_Id FROM event_stories JOIN 
    user ON user.Id = event_stories.User_Id WHERE event_stories.Event_Id = ${event_id} AND
     (event_stories.User_Id IN (SELECT user.Id FROM friend,user WHERE (user_one_id = ${user_id} AND user.Id = friend.user_two_id OR user_two_id = ${user_id}
     AND user.Id = friend.user_one_id) AND status = 1) OR event_stories.User_Id = ${user_id}) 
    UNION
    (SELECT user.Id,user.Username,user.Profile,NULL as Stories_Id FROM event_user
    INNER JOIN user
    INNER JOIN friend
    INNER JOIN event
    WHERE ((user_one_id = ${user_id} AND 
    user.Id = friend.user_two_id OR user_two_id = ${user_id} AND user.Id = friend.user_one_id) AND status = 1) AND event_user.User_Id = user.Id AND event_user.Event_Id = event.Id AND event.Id = ${event_id})) as table1
    GROUP BY table1.Id`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});

router.get('/:event_id/all', async function(req, res) {
    let event_id = con.escape(req.params.event_id);
    await con.query(`SELECT user.Id, user.Username, user.Profile, event_stories.Stories_Id FROM event_stories INNER JOIN stories ON stories.Id = event_stories.Stories_Id INNER JOIN user ON user.Id = event_stories.User_Id WHERE event_stories.Event_Id = ${event_id} GROUP BY id`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});

//Get User All Stories by user_id
router.get('/getall/:user_id/:event_id', async function(req, res) {
    let user_id = con.escape(req.params.user_id);
    let event_id = con.escape(req.params.event_id);
    await con.query(`SELECT stories.*,user.Verify FROM event_stories INNER JOIN user ON user.Id = event_stories.User_Id INNER JOIN stories ON stories.Id = event_stories.Stories_Id WHERE event_stories.User_Id = ${user_id} AND event_stories.Event_Id = ${event_id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});


router.get('/count/:event_id', async function(req, res) {
    let event_id = con.escape(req.params.event_id);
    await con.query(`SELECT COUNT(*) as Count FROM event_stories WHERE event_stories.Event_Id = ${event_id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        if (result[0].Count > 0) {
            res.status(200).send(JSON.stringify({
                "result": "true",
                "message": "have story"
            }));
        } else {
            res.status(200).send(JSON.stringify({
                "result": "false",
                "message": "dont have story"
            }));
        }
    });
});


router.post('/create', async function(req, res) {
    var form = new formidable.IncomingForm();

    form.parse(req);
    var lat = con.escape(req.query.latitude);
    if (typeof req.query.latitude !== 'undefined') {
        lat = con.escape(req.query.latitude);
    } else {
        lat = 0;
    }
    var lon = con.escape(req.query.longitude);
    if (typeof req.query.longitude !== 'undefined') {
        lon = con.escape(req.query.longitude);
    } else {
        lon = 0;
    }
    var ele = con.escape(req.query.elevation);
    if (typeof req.query.elevation !== 'undefined') {
        ele = con.escape(req.query.elevation);
    } else {
        ele = 0;
    }

    await form.on('fileBegin', function(name, file) {
        var pathName = Date.now() + file.name.replace(/\s/g, '');
        file.path = __dirname + '/../public/uploads/stories/' + pathName;
        file.name = pathName;
    });

    await form.on('file', async function(name, file) {
        var fullUrl = req.protocol + '://' + req.get('host') + "/images/stories/";
        console.log(req.query.color_id);
        await con.query(`INSERT INTO stories (Picture, Title, Color,Latitude,Longitude,Elevation) VALUES ('${fullUrl + file.name}', ${con.escape(req.query.title)} , ${con.escape(req.query.color_id)}, ${lat},${lon},${ele})`, async function(err, result, fields) {
            if (err) {
                console.log(err);
                res.status(400).send(JSON.stringify({
                    "result": "error",
                    "message": err.message
                }));
            }
            await con.query(`INSERT INTO event_stories (Event_Id, User_Id, Stories_Id) VALUES (${con.escape(req.query.event_id)}, ${req.user.id}, ${result.insertId});`, function(err, result, result) {
                if (err) {
                    console.log(err);
                    res.status(400).send(JSON.stringify({
                        "result": "error",
                        "message": err.message
                    }));
                }
                con.query(`INSERT INTO notification (Title,Hashtag,Type,Picture,User_Id_from,User_Id_to,Action_Id)
                    SELECT 'shared story:',${con.escape(req.query.title)},4,'${fullUrl + file.name}',${req.user.id},user.Id,${con.escape(req.query.event_id)} FROM friend,user WHERE 
                    (user_one_id = ${con.escape(req.user.id)} AND user.Id = friend.user_two_id OR user_two_id = ${con.escape(req.user.id)}
                        AND user.Id = friend.user_one_id) AND status = 1`, function(err, result, fields) {
                    if (err) console.log(err);
                });
                con.query(`SELECT fcm_notification.FirebaseInstanceId FROM fcm_notification,friend,user WHERE (user_one_id = ${req.user.id} AND user.Id = friend.user_two_id OR user_two_id = ${req.user.id} AND user.Id = friend.user_one_id) AND status = 1 AND fcm_notification.User_Id = user.Id`, function(err, result, fields) {
                    if (err) console.log(err);
                    else if (result.length > 0) fcm_notification(req.user.username, "shared story:", con.escape(req.query.title), "4", `"${req.user.id}"`, "", Array.from(result, x => x.FirebaseInstanceId), "high");
                });
                res.status(201).send(JSON.stringify({
                    "result": "success",
                    "message": "Added Story"
                }));
            });
        });
    });

});


module.exports = router;