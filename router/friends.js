var express = require('express');
var router = express.Router();
var path = require('path')
var con = require(path.join('../db.js'));
var fcm_notification = require('../utils/notification.js');

//Friends Request
router.post('/request', async function(req, res) {
    var user_one_id = req.user.id;
    var user_two_id = req.body.user_two_id;

    await con.query("INSERT INTO `friend` (`user_one_id`, `user_two_id`, `status`, `action_user_id`) VALUES (?, ?, 0, ?) ON DUPLICATE KEY UPDATE status = 0", [user_one_id, user_two_id, user_one_id], async function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        con.query(`SELECT fcm_notification.FirebaseInstanceId FROM fcm_notification WHERE fcm_notification.User_Id = ${user_two_id}`, function(err, result, fields) {
            if (err) console.log(err);
            else if (result.length > 0) fcm_notification(req.user.username, "sent you friendship", "", "2", `"${user_one_id}"`, "", Array.from(result, x => x.FirebaseInstanceId), "normal");
        });
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Send Request to User!"
        }));
    });
});

//Accept Friend Request 
router.post('/accept/request', async function(req, res) {
    var user_one_id = req.user.id;
    var user_two_id = req.body.user_two_id;

    await con.query("UPDATE `friend` SET `status` = 1, `action_user_id` = ? WHERE `user_one_id` = ? AND `user_two_id` = ?", [user_one_id, user_two_id, user_one_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        con.query(`SELECT fcm_notification.FirebaseInstanceId FROM fcm_notification WHERE fcm_notification.User_Id = ${user_two_id}`, function(err, result, fields) {
            if (err) console.log(err);
            else if (result.length > 0) fcm_notification(req.user.username, "has become your new friend", "", "2", `"${user_one_id}"`, "", Array.from(result, x => x.FirebaseInstanceId), "high");
        });
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Accept Request to User!"
        }));
    });
});
//Cancel Friend Request
router.post('/decline/request', async function(req, res) {
    var user_one_id = req.user.id;
    var user_two_id = req.body.user_two_id;

    await con.query("DELETE FROM friend WHERE (user_one_id = ? AND user_two_id = ?) OR  (user_one_id = ? AND user_two_id = ?)", [user_two_id, user_one_id, user_one_id, user_two_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Decline Request to User!"
        }));
    });
});

//Checking Friendship
router.post('/check', async function(req, res) {
    var user_one_id = req.user.id;
    var user_two_id = req.body.user_two_id;

    await con.query("SELECT COUNT(*) AS Count FROM `friend` WHERE `user_one_id` = ? AND `user_two_id` = ? AND `status` = 1", [user_one_id, user_two_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        if (result[0].Count > 0) {
            res.status(200).send(JSON.stringify({
                "result": "friend",
                "message": "User is your friend!"
            }));
        } else {
            res.status(200).send(JSON.stringify({
                "result": "no-friend",
                "message": "User isn't your friend!"
            }));
        }
    });
});
//Friends List
router.get('/list', async function(req, res) {
    var user_id = req.user.id;
    await con.query("SELECT user.Id,user.Username,user.Verify,user.Profile,1 AS isFriend  FROM `friend`,user WHERE (`user_one_id` = ? AND user.Id = friend.user_two_id OR `user_two_id` = ? AND user.Id = friend.user_one_id) AND `status` = 1", [user_id, user_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});
//Pending Request
router.get('/pending/list', async function(req, res) {
    var user_id = con.escape(req.user.id);
    var username = con.escape("%" + req.query.username + "%");
    await con.query(`SELECT * FROM (SELECT user.Id,user.Username,user.Verify,user.Profile,'0' AS isFriend
    FROM user
    WHERE user.Username LIKE ${username} AND user.Id IN ( SELECT IF(friend.user_one_id = user.Id , friend.user_one_id, friend.user_two_id) FROM friend
                          WHERE ((friend.user_one_id = user.Id AND friend.user_two_id = ${user_id}) OR (friend.user_two_id = user.Id AND friend.user_one_id =  ${user_id}))
                          AND friend.status = 0 AND friend.action_user_id = user.Id)) AS UsersList WHERE UsersList.Id <>  ${user_id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});
//Search List
router.get('/search', async function(req, res) {
    var username = con.escape("%" + req.query.username + "%");
    var user_id = con.escape(req.user.id);

    await con.query(`SELECT * FROM (SELECT user.Id,user.Verify,user.Username,user.Profile,'2' AS isFriend 
    FROM user
    WHERE user.Username LIKE ${username} AND user.Id NOT IN ( SELECT IF(friend.user_one_id = user.Id , friend.user_one_id, friend.user_two_id) FROM friend
                          WHERE ((friend.user_one_id = user.Id AND friend.user_two_id = ${user_id}) OR (friend.user_two_id = user.Id AND friend.user_one_id = ${user_id}))
                          AND (friend.status <> 2))
    UNION
    SELECT user.Id,user.Verify,user.Username,user.Profile,'0' AS isFriend
    FROM user
    WHERE user.Username LIKE ${username} AND user.Id IN ( SELECT IF(friend.user_one_id = user.Id , friend.user_one_id, friend.user_two_id) FROM friend
                          WHERE ((friend.user_one_id = user.Id AND friend.user_two_id = ${user_id}) OR (friend.user_two_id = user.Id AND friend.user_one_id = ${user_id}))
                          AND friend.status = 0 AND friend.action_user_id = ${user_id})) AS UsersList WHERE UsersList.Id <> ${user_id} ORDER BY UsersList.Id `, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    })
});

//Get Event From user
router.get('/events', async function(req, res) {
    var user_id = con.escape(req.query.user_id);
    var event_id = con.escape(req.query.event_id);
    console.log(user_id);
    console.log(event_id);
    await con.query(`SELECT user.Id,user.Username,user.Verify,user.Profile,0 AS isFriend FROM user
    WHERE user.Id IN (SELECT IF(friend.user_one_id = user.Id ,friend.user_one_id,friend.user_two_id)
    FROM friend,event_user WHERE event_user.Event_Id = ${event_id} AND ((user_one_id = ${user_id} AND event_user.User_Id = friend.user_two_id)
    OR (user_two_id = ${user_id} AND event_user.User_Id = friend.user_one_id)) AND status = 1)
    AND user.Id <> ${user_id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result);
    });
});

//Friend request sent by the user
router.post('/visit', async function(req, res) {
    var user_one_id = req.body.user_one_id;
    var user_two_id = req.body.user_two_id;
    await con.query("SELECT friend.status FROM `friend` WHERE `user_one_id` = ? AND `user_two_id` = ?", [user_id, user_two_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        res.status(200).send(result[0].status == 0);
    });
});

// SELECT * FROM user
// WHERE
// (SELECT COUNT(*) FROM friend WHERE ((friend.user_one_id = 85 AND friend.user_two_id = user.Id) OR (friend.user_two_id= 85 AND friend.user_one_id = user.Id))
// AND friend.status = 1) > 0


module.exports = router;