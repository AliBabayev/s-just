var express = require('express');
var router = express.Router();
var formidable = require('formidable');
var path = require('path')
var con = require(path.join('../db.js'));
var bearing = require('./../utils/bearing');
router.get('/all', async function(req, res) {
    var sqlQuery = `SELECT Id, Picture, Lat, Lon, Category_Id from event`;
    await con.query(sqlQuery, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/today', async function(req, res) {
    var hashtag = con.escape(req.query.SearchT);
    var timezone_app = req.query.TimeZone;
    var sqlQuery = `SELECT row.Id, row.Picture, row.Lat, row.Lon, row.Category_Id, row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE CONVERT_TZ(NOW(), @@GLOBAL .time_zone, @@GLOBAL .time_zone) BETWEEN CONVERT_TZ(event.Date, @@GLOBAL .time_zone, @@GLOBAL .time_zone) AND CONVERT_TZ(event.Date2, @@GLOBAL .time_zone, @@GLOBAL .time_zone) UNION SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, 0 AS Category_Id, Lat, Lon FROM event WHERE (CONVERT_TZ(NOW(), @@GLOBAL .time_zone, @@GLOBAL .time_zone) NOT BETWEEN CONVERT_TZ(event.Date, @@GLOBAL .time_zone, @@GLOBAL .time_zone) AND CONVERT_TZ(event.Date2, @@GLOBAL .time_zone, @@GLOBAL .time_zone)) AND DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, @@GLOBAL .time_zone)) = DATE(CONVERT_TZ(NOW(), @@GLOBAL .time_zone, @@GLOBAL .time_zone))) AS row WHERE INSTR(row.Hashtag, ${hashtag} ) > 0`
    if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '0') {
        timezone_app = '-' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5); //10434
    } else if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '1') {
        timezone_app = '+' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5); //10434
    }
    if (typeof timezone_app !== 'undefined' && timezone_app.length !== 0)
        sqlQuery = `SELECT row.Id, row.Picture, row.Lat, row.Lon, row.Category_Id, row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}) BETWEEN CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) AND CONVERT_TZ(event.Date2, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) UNION SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, 0 AS Category_Id, Lat, Lon FROM event WHERE (CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}) NOT BETWEEN CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) AND CONVERT_TZ(event.Date2, @@GLOBAL .time_zone, ${con.escape(timezone_app)})) AND DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)})) = DATE(CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}))) AS row WHERE INSTR(row.Hashtag, ${hashtag}) > 0`
    if (req.query.Type != -1) {
        sqlQuery += ` AND row.Type = ${con.escape(req.query.Type)}`;
    }
    await con.query(sqlQuery, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/tomorrow', async function(req, res) {
    var hashtag = con.escape(req.query.SearchT);
    var timezone_app = req.query.TimeZone;
    var sqlQuery = `SELECT row.Id, row.Picture, row.Lat, row.Lon, 0 AS 'Category_Id',row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, @@GLOBAL .time_zone)) = DATE(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 DAY), @@GLOBAL .time_zone, @@GLOBAL .time_zone))) AS row WHERE INSTR(row.Hashtag, ${hashtag}) > 0`;
    if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '0') {
        timezone_app = '-' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5);
    } else if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '1') {
        timezone_app = '+' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5);
    }
    if (typeof timezone_app !== 'undefined' && req.query.TimeZone.length !== 0)
        sqlQuery = `SELECT row.Id, row.Picture, row.Lat, row.Lon, 0 AS 'Category_Id', row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)})) = DATE(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 DAY), @@GLOBAL .time_zone, ${con.escape(timezone_app)}))) AS row WHERE INSTR(row.Hashtag, ${hashtag}) > 0`;
    if (req.query.Type != -1) {
        sqlQuery += ` AND row.Type = ${con.escape(req.query.Type)}`;
    }
    await con.query(sqlQuery, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/week', async function(req, res) {
    var hashtag = con.escape(req.query.SearchT);
    var timezone_app = req.query.TimeZone;
    var sqlQuery = `SELECT row.Id, row.Picture, row.Lat, row.Lon, 0 AS 'Category_Id',row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, @@GLOBAL .time_zone)) BETWEEN DATE(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 2 DAY), @@GLOBAL .time_zone, @@GLOBAL .time_zone)) AND DATE(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 7 DAY), @@GLOBAL .time_zone, @@GLOBAL .time_zone))) AS row WHERE INSTR(row.Hashtag, ${hashtag}) > 0`;
    if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '0') {
        timezone_app = '-' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5);
    } else if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '1') {
        timezone_app = '+' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5);
    }
    if (typeof timezone_app !== 'undefined' && req.query.TimeZone.length !== 0)
        sqlQuery = `SELECT row.Id, row.Picture, row.Lat, row.Lon, 0 AS 'Category_Id',row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)})) BETWEEN DATE(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 2 DAY), @@GLOBAL .time_zone, ${con.escape(timezone_app)})) AND DATE(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 7 DAY), @@GLOBAL .time_zone, ${con.escape(timezone_app)}))) AS row WHERE INSTR(row.Hashtag, ${hashtag}) > 0`;
    if (req.query.Type != -1) {
        sqlQuery += ` AND row.Type = ${con.escape(req.query.Type)}`;
    }
    await con.query(sqlQuery, function(err, result, fields) {
        if (err) res.status(200).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/close', async function(req, res) {
    var lat = req.query.lat;
    var lon = req.query.lon;
    var ele = req.query.ele;

    var timezone_app = req.query.TimeZone;
    if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '0') {
        timezone_app = '-' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5); //10434
    } else if (typeof timezone_app !== 'undefined' && req.query.TimeZone[0] === '1') {
        timezone_app = '+' + timezone_app.slice(1, 3) + ':' + timezone_app.slice(3, 5); //10434
    } else {
        timezone_app = "@@GLOBAL .time_zone";
    }
    var eventsQuery = `SELECT row.Id,row.Title,row.Description,row.HashTag,row.Cover, row.Picture, row.Lat, row.Lon, row.Category_Id, row.Type AS 'Type' FROM (SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, Category_Id, Lat, Lon FROM event WHERE CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}) BETWEEN CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) AND CONVERT_TZ(event.Date2, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) UNION SELECT Id, Title, Description, Hashtag, Date, Date2, Picture, Cover, Created_Id, type, 0 AS Category_Id, Lat, Lon FROM event WHERE (CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}) NOT BETWEEN CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) AND CONVERT_TZ(event.Date2, @@GLOBAL .time_zone, ${con.escape(timezone_app)})) AND DATE(CONVERT_TZ(event.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)})) = DATE(CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}))) AS row WHERE INSTR(row.Hashtag, '') > 0`
    var closeEventQuery = `SELECT * FROM (SELECT TodayEvent.*,ST_Distance_Sphere(point(TodayEvent.Lat, TodayEvent.Lon),point(${con.escape(lat)},${con.escape(lon)}))/1000 as Distance FROM (${eventsQuery}) as TodayEvent) AS EventsNew WHERE EventsNew.Distance <1.1 ORDER BY EventsNew.Distance DESC`;

    var storiesQuery = `SELECT * FROM stories WHERE CONVERT_TZ(NOW(), @@GLOBAL .time_zone, ${con.escape(timezone_app)}) BETWEEN CONVERT_TZ(stories.Date, @@GLOBAL .time_zone, ${con.escape(timezone_app)}) AND CONVERT_TZ(DATE_ADD(stories.Date, INTERVAL 1 DAY), @@GLOBAL .time_zone, ${con.escape(timezone_app)})`;
    var closeStoriesQuery = `SELECT * FROM (SELECT CloseStories.*,ST_Distance_Sphere(point(CloseStories.Latitude, CloseStories.Longitude),point(${con.escape(lat)},${con.escape(lon)}))/1000 as Distance FROM (${storiesQuery}) as CloseStories) AS CloseStoriesNew WHERE CloseStoriesNew.Distance <1.1 ORDER BY CloseStoriesNew.Distance DESC`;

    var closeUsersQuery = `SELECT * FROM (SELECT user.Id,user.Firstname,user.Lastname,user.Username,user.About,user.Profile,user.Verify,user.Latitude,user.Longitude,user.Elevation,ST_Distance_Sphere(point(user.Latitude, user.Longitude),point(${con.escape(lat)},${con.escape(lon)}))/1000 as Distance FROM user) AS CloseUsers WHERE CloseUsers.Distance <1.1  AND CloseUsers.Id <> ${req.user.id} ORDER BY CloseUsers.Distance DESC `

    var updateLatLon = `UPDATE user SET user.Latitude = ${con.escape(lat)}, user.Longitude = ${con.escape(lon)}, user.Elevation = ${con.escape(ele)}, user.last_updated = NOW() WHERE user.Id = ${req.user.id}`;
    con.query(`${closeEventQuery};${closeStoriesQuery}; ${closeUsersQuery};${updateLatLon}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else {
            result[0].map(item => {
                item.BearingX = bearing.bearingX(lat, lon, item.Lat, item.Lon);
                item.BearingY = 90;
            });
            result[1].map(item => {
                item.BearingX = bearing.bearingX(lat, lon, item.Latitude, item.Longitude);
                item.BearingY = bearing.bearingY(ele, item.Elevation, item.Distance);
            });
            result[2].map(item => {
                item.BearingX = bearing.bearingX(lat, lon, item.Latitude, item.Longitude);
                item.BearingY = bearing.bearingY(ele, item.Elevation, item.Distance);
            });
            res.status(200).send({
                "CloseEvents": result[0],
                "CloseStories": result[1],
                "CloseUsers": result[2],
                "UpdateUser": (result[3].changedRows == 1 ? {
                    "result": "changed",
                    "message": result[3].changedRows
                } : {
                    "result": "unchanged",
                    "message": result[3].changedRows
                })
            });
        }
    });
});

router.get('/ht/:hashtag', async function(req, res) {
    let hashtag = req.params.hashtag;
    await con.query("SELECT * FROM event WHERE event.Hashtag = ?", hashtag, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/:id/created_id', async function(req, res) {
    let id = req.params.id;
    await con.query("SELECT user.Id,user.Username,user.Profile,user.Address,user.About FROM user INNER JOIN event ON event.Id = ? and event.Created_Id = user.Id", id, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result[0]);
    });
});

router.get('/short/:id', async function(req, res) {
    let id = req.params.id;
    await con.query("SELECT event.Id,event.Title,LEFT(event.Description,70) AS Description,event.Hashtag,event.Picture,event.Cover FROM event WHERE event.Id = ?", id, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        if (result[0] != null)
            con.query("UPDATE event SET event.ViewCount = event.ViewCount + 1 WHERE event.Id = " + con.escape(id) + ";");
        res.status(200).send(result[0]);
    });
});

router.get('/:id', async function(req, res) {
    let id = req.params.id;
    await con.query("SELECT * FROM event WHERE event.Id = ?", id, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        if (result[0] != null)
            con.query("UPDATE event SET event.ViewCount = event.ViewCount + 1 WHERE event.Id = " + con.escape(id) + ";");
        res.status(200).send(result[0]);
    });
});

router.get('/:id/pictures', async function(req, res) {
    let id = req.params.id;
    await con.query("SELECT * FROM event_picture WHERE Event_Id = ?", id, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/filter/:id', async function(req, res) {
    let id = req.params.id;
    await con.query("SELECT * from event WHERE event.Type = ?", id, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result[0]);
    });
});

router.get('/fullinfo/:id', async function(req, res) {
    await con.query("SELECT * FROM event WHERE event.Id = ? ", [req.params.id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});


router.post('/bookmark/add', async function(req, res) {
    var event_id = req.body.id;
    await con.query("INSERT INTO event_bookmark(User_Id,Event_Id) VALUES(?,?)", [req.user.id, event_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "1"
        }));;
    });
});

router.post('/bookmark/remove', async function(req, res) {
    var event_id = req.body.id;
    await con.query("DELETE FROM event_bookmark WHERE event_bookmark.User_Id = ? AND event_bookmark.Event_Id = ?", [req.user.id, event_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "1"
        }));;
    });
});

router.get('/bookmark/all', async function(req, res) {
    await con.query("SELECT * FROM event_bookmark WHERE event_bookmark.User_Id = ?", [req.user.id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});


router.get('/:id/bookmark/check', async function(req, res) {
    var event_id = req.params.id;
    await con.query("SELECT COUNT(*) AS Count FROM event_bookmark WHERE event_bookmark.User_Id = ? AND event_bookmark.Event_Id = ?", [req.user.id, event_id], function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        if (result[0].Count == 1)
            res.status(201).send(JSON.stringify({
                "result": "success",
                "message": "1"
            }));
        else
            res.status(201).send(JSON.stringify({
                "result": "success",
                "message": "0"
            }));
    });
});

router.post('/create', async function(req, res) {
    let event = req.body;
    var myDate = new Date(event.Date);
    var dateEvent = myDate.toJSON().slice(0, 19).replace('T', ' ');
    var myDate1 = new Date(event.Date2);
    var dateEvent2 = myDate1.toJSON().slice(0, 19).replace('T', ' ');
    await con.query(`INSERT INTO event (Id, Title,Category_Id, Description, Hashtag, Date, Date2, Picture, Cover, Thumb, Created_Id, Type, Lat, Lon) 
                          VALUES (NULL, ${con.escape(event.Title)},${con.escape(event.Category_Id)}, ${con.escape(event.Description)}, ${con.escape(event.Hashtag)}, ${con.escape(dateEvent)} , ${con.escape(dateEvent2)} , ${con.escape(event.Picture)}, ${con.escape(event.Cover)}, ${con.escape(event.Thumb)}, ${con.escape(req.user.id)}, ${con.escape(event.Type)}, ${con.escape(event.Lat)}, ${con.escape(event.Lon)})`, async function(err, result, fields) {
        if (err) {
            res.status(400).send(JSON.stringify({
                "result": "error",
                "message": err.message
            }));
        }
        con.query(`SELECT fcm_notification.FirebaseInstanceId FROM fcm_notification,friend,user WHERE (user_one_id = ${req.user.id} AND user.Id = friend.user_two_id OR user_two_id = ${req.user.id} AND user.Id = friend.user_one_id) AND status = 1 AND fcm_notification.User_Id = user.Id`, function(err, result, fields) {
            if (err) console.log(err);
            else if (result.length > 0) fcm_notification(req.user.username, "created an event", event.Hashtag, "1", `"${result.insertId}"`, "", Array.from(result, x => x.FirebaseInstanceId), "high");
        });
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Created Event"
        }));
        if (event.Ticket_List.length > 0) {
            let stmt = `INSERT INTO ticket (Title, Description, Profile, Link, Event_Id) VALUES ?`;
            // execute the insert statment
            con.query(stmt, [event.Ticket_List.map(item => [item.Title, item.Description, item.Profile, item.Link, result.insertId])], (err, results, fields) => {
                if (err) {
                    console.log(err);
                }
            });
        }
    })
});


router.get('/follow/check', async function(req, res) {
    var e_id = con.escape(req.query.Event_Id);
    var u_id = con.escape(req.user.id);
    await con.query("SELECT COUNT(*) AS Count FROM event_user WHERE event_user.User_Id = " + u_id + " AND event_user.Event_Id = " + e_id, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        if (result[0].Count > 0) {
            res.status(201).send(JSON.stringify({
                "result": "FOLLOW",
                "message": "This user have"
            }));
        } else {
            res.status(201).send(JSON.stringify({
                "result": "UNFOLLOW",
                "message": "This user dont have"
            }));
        }
    });
});

router.post('/follow/check', async function(req, res) {
    await con.query("INSERT INTO event_user (Id, User_Id, Event_Id) VALUES (NULL, " + con.escape(req.user.id) + " , " + con.escape(req.body.Event_Id) + ");", function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Following"
        }));
    });
});

router.post('/follow/uncheck', async function(req, res) {
    await con.query("DELETE FROM event_user WHERE event_user.User_Id = " + con.escape(req.user.id) + " AND event_user.Event_Id = " + con.escape(req.body.Event_Id), function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "UnFollowing"
        }));
    });
});

router.get('/random/:count', async function(req, res) {
    let count = con.escape(req.params.count);
    console.log(count);
    await con.query("SELECT * FROM event ORDER BY RAND() LIMIT " + count, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    })
})

router.get('/live/:count', async function(req, res) {
    let count = req.params.count;
    await con.query(`SELECT stories.Id as Id, user.Username, user.Profile, user.Verify, stories.Date ,stories.Picture,stories.Title,event.Id as EventId FROM event INNER JOIN event_stories ON event_stories.Event_Id = event.Id INNER JOIN user ON event_stories.User_Id = user.Id INNER JOIN stories ON stories.Id = event_stories.Stories_Id ORDER BY stories.Date DESC LIMIT ${con.escape(25 * count)}, 25`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.get('/:event_id/tickets', async function(req, res) {
    let event_id = con.escape(req.params.event_id);
    await con.query(`SELECT * FROM ticket WHERE ticket.Event_Id = ${event_id} `, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});

router.delete('/:event_id', async function(req, res) {
    let event_id = con.escape(req.params.event_id);
    await con.query(`DELETE FROM events WHERE events.Id = ${event_id}`, async function(req, res) {
        if (err) res.status(400).send(err.message);
        else res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Deleted event"
        }));
    });
});

router.put('/update', async function(req, res) {
    let event = req.body;
    var myDate = new Date(event.Date);
    var dateEvent = myDate.toJSON().slice(0, 19).replace('T', ' ');
    var myDate1 = new Date(event.Date2);
    var dateEvent2 = myDate1.toJSON().slice(0, 19).replace('T', ' ');
    await con.query(`UPDATE event SET Title = ${con.escape(event.Title)},Category_Id = ${con.escape(event.Category_Id)} , Description = ${con.escape(event.Description)}, Hashtag = ${con.escape(event.Hashtag)}, Date = ${con.escape(dateEvent)}, Date2 = ${con.escape(dateEvent2)}, Picture = ${con.escape(event.Picture)}, Cover = ${con.escape(event.Cover)}, Thumb = ${con.escape(event.Thumb)}, Created_Id = ${con.escape(req.user.id)}, Type = ${con.escape(event.Type)}, Lat = ${con.escape(event.Lat)}, Lon = ${con.escape(event.Lon)} WHERE event.Id = ${event.Id}`, async function(err, result, fields) {
        if (err) {
            res.status(400).send(JSON.stringify({
                "result": "error",
                "message": err.message
            }));
        } else res.status(201).send(JSON.stringify({
            "result": "success",
            "message": "Successfuly Updated Event"
        }));

        con.query(`DELETE FROM ticket WHERE ticket.Event_Id = ${event.Id} `, function(err, results, fields) {
            if (err) {
                console.log('dont inserted' + err);
            }
            if (event.Ticket_List.length > 0) {
                let stmt = `INSERT INTO ticket (Title, Description, Profile, Link, Event_Id) VALUES ? `;
                // execute the insert statment
                con.query(stmt, [event.Ticket_List.map(item => [item.Title, item.Description, item.Profile, item.Link, event.Id])], (err, results, fields) => {
                    if (err) {
                        console.log('dont inserted' + err);
                    }
                });
            }
        });


    })
});

router.delete('/delete/:event_id', async function(req, res) {
    let event_id = req.params.event_id;
    con.query(`DELETE FROM event WHERE event.Id = ${event_id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(201).send(result);
    });
});


router.get('/me/created', async function(req, res) {
    let user_id = con.escape(req.user.id);
    await con.query(`SELECT * FROM event WHERE event.Created_Id = ${user_id}`, function(err, result, fields) {
        if (err) res.status(400).send(err.message);
        else res.status(200).send(result);
    });
});


router.post('/upload', async function(req, res) {
    var form = new formidable.IncomingForm();

    form.parse(req);
    var imgPathArray = new Array();
    await form.on('fileBegin', function(name, file) {
        var pathName = Date.now() + file.name.replace(/\s/g, '');
        file.path = __dirname + '/../public/uploads/events/' + pathName;
        file.name = pathName;
    });

    await form.on('file', function(name, file) {
        var fullUrl = req.protocol + '://' + req.get('host') + "/images/events/";
        imgPathArray.push(fullUrl + file.name);
        console.log('Uploaded ' + fullUrl + file.name);
    });

    await form.on('end', function(name, file) {
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": imgPathArray[0] + ',' + imgPathArray[1]
        }));
    })
});

router.post('/upload/image', async function(req, res) {
    var form = new formidable.IncomingForm();

    form.parse(req);
    await form.on('fileBegin', function(name, file) {
        var pathName = Date.now() + file.name.replace(/\s/g, '');
        file.path = __dirname + '/../public/uploads/events/' + pathName;
        file.name = pathName;
    });

    await form.on('file', function(name, file) {
        var fullUrl = req.protocol + '://' + req.get('host') + "/images/events/";
        console.log('Uploaded ' + fullUrl + file.name);
        res.status(201).send(JSON.stringify({ "result": "success", "message": fullUrl + file.name }));
    });
});


router.post('/upload/thumb', async function(req, res) {
    var form = new formidable.IncomingForm();

    form.parse(req);
    await form.on('fileBegin', function(name, file) {
        var pathName = Date.now() + file.name.replace(/\s/g, '');
        file.path = __dirname + '/../public/uploads/events_tmb/' + pathName;
        file.name = pathName;
    });

    await form.on('file', function(name, file) {
        var fullUrl = req.protocol + '://' + req.get('host') + "/images/events/";
        console.log('Uploaded ' + fullUrl + file.name);
        res.status(201).send(JSON.stringify({ "result": "success", "message": fullUrl + file.name }));
    });
});

router.get('/list/going/:event_id', async function(req, res) {
    let event_id = req.params.event_id;
    let user_id = req.user.id;
    con.query(`SELECT concat(user.firstname," ",user.lastname) FROM event_user,user WHERE user.Id = event_user.User_Id AND event_user.Event_id = ${event_id} AND event_user.User_id IN (SELECT user.Id,user.Username,user.Verify,user.Profile,1 AS isFriend  FROM friend,user WHERE (user_one_id = ${user_id} AND user.Id = friend.user_two_id OR user_two_id = ${user_id} AND user.Id = friend.user_one_id) AND status = 1)`, function(err, result, fields) {
        if (err) {
            res.status(400).send(JSON.stringify({
                "result": "error",
                "message": err.message
            }));
        }
        res.status(201).send(result);
    })
});

router.get('/list/days/:event_id', async function(req, res) {
    let event_id = req.params.event_id;
    await con.query(`SELECT event.Id, event.Title,event.Description,event.Cover,event.Date,event.Date2 FROM event_locations,event WHERE event_locations.day_id=${event_id} AND event_locations.event_id = event.Id`, function(err, result, fields) {
        if (err) {
            res.status(400).send(JSON.stringify({
                "result": "error",
                "message": err.message
            }));
        }
        res.status(201).send(result);
    });
});

module.exports = router;