var express = require('express');
var router = express.Router();
var path = require('path')
var con = require(path.join('../db.js'));
var jwt = require('jsonwebtoken');
var verifyToken = require('../middleware/verifyToken.js');


router.post('/login', async function(req, res) {
    let emp = req.body;
    console.log(emp);
    await con.query(`SELECT * FROM user WHERE (Email = ${con.escape(emp.Email)} OR Username = ${con.escape(emp.Email)}) AND Password = ${con.escape(emp.Password)}`, function(err, result) {
        if (err) res.status(400).send(err);
        if (result[0] != null) {
            con.query("UPDATE user SET user.last_signing = current_timestamp() WHERE user.Id = " + result[0].Id + ";");
            var token = jwt.sign({ id: result[0].Id, username: result[0].Username, email: result[0].Email, password: result[0].Password }, process.env.SECRET_KEY);
            result[0].Token = token;
            if (emp.FirebaseInstanceId != '' && emp.FirebaseInstanceId != undefined && emp.Device_Id != undefined)
                con.query(`INSERT INTO fcm_notification(User_Id,FirebaseInstanceId,Device_Id,Device_Type,Last_Used) VALUES(${result[0].Id},${con.escape(emp.FirebaseInstanceId)},${con.escape(emp.Device_Id)},'Android',NOW()) ON DUPLICATE KEY UPDATE User_Id = ${result[0].Id} , FirebaseInstanceId = ${con.escape(emp.FirebaseInstanceId)},Last_Used = NOW()`, function(err, result, fields) {
                    if (err) console.log(err);
                });
            res.status(200).send(result[0]);
        } else {
            res.status(400).send("Username or email is not valid");
        }
    });
});

router.post('/loginauto', verifyToken, async function(req, res) {
    var emp = {
        Email: req.user.email,
        Username: req.user.username,
        Password: req.user.password,
        FirebaseInstanceId: req.body.FirebaseInstanceId,
        Device_Id: req.body.Device_Id
    }
    console.log(emp);
    await con.query(`SELECT * FROM user WHERE Email = ${con.escape(emp.Email)} AND Password = ${con.escape(emp.Password)}`, function(err, result) {
        if (err) res.status(400).send(err);
        if (result[0] != null) {
            con.query("UPDATE user SET user.last_signing = current_timestamp() WHERE user.Id = " + result[0].Id + ";");
            var token = jwt.sign({ id: result[0].Id, username: result[0].Username, email: result[0].Email, password: result[0].Password }, process.env.SECRET_KEY);
            result[0].Token = token;
            if (emp.FirebaseInstanceId != '' && emp.FirebaseInstanceId != undefined && emp.Device_Id != undefined)
                con.query(`INSERT INTO fcm_notification(User_Id,FirebaseInstanceId,Device_Id,Device_Type,Last_Used) VALUES(${result[0].Id},${con.escape(emp.FirebaseInstanceId)},${con.escape(emp.Device_Id)},'Android',NOW()) ON DUPLICATE KEY UPDATE User_Id = ${result[0].Id} , FirebaseInstanceId = ${con.escape(emp.FirebaseInstanceId)},Last_Used = NOW()`, function(err, result, fields) {
                    if (err) console.log(err);
                });
            res.status(200).send(result[0]);
        } else {
            res.status(400).send("Username or email is not valid");
        }
    });
});

router.post('/register', async function(req, res) {
    let user = req.body;
    console.log(req.body);
    await con.query(`SELECT COUNT(*) as COUNT FROM user WHERE user.Username = ${con.escape(user.Username)} OR user.Email = ${con.escape(user.Email)}`, function(err, result, fields) {
        if (result[0].COUNT > 0) {
            res.status(200).send(JSON.stringify({
                "result": "error_email_validation",
                "message": "Username or Email already exist"
            }));
        } else {
            con.query(`INSERT INTO user (Id, Username, Password, Email, FirstName, LastName, Facebook,Twitter,Pinterest, About,Address,Profile)
        VALUES(NULL, ? , ? , ? , NULL, NULL ,NULL ,NULL, NULL, ? , ? , ?)`, [user.Username, user.Password, user.Email, user.About, user.Address, user.Profile], function(err, result, fields) {
                if (err) console.log(err);
                console.log(result[0]);
                res.status(201).send(JSON.stringify({
                    "result": "success",
                    "message": "Successfully Created User"
                }));
            });
        }
    });
});

module.exports = router;