var mysql = require('mysql');
var util = require('util');
// var con = mysql.createConnection({
//     host: "db4free.net",
//     user: "gameover",
//     password: "0772209966a",
//     database: "justdb",
//     port: 3306,
//     timezone: 'utc' 
// });

var con = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT,
    ssl: true,
    timezone: process.env.DB_TIME_ZONE,
    multipleStatements: true
});

con.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
    }
    if (connection) connection.release()
    return
})

con.query = util.promisify(con.query);


// var con = mysql.createConnection({
// host: process.env.LDB_HOST,
// user: process.env.LDB_USER,
// password: process.env.LDB_PASSWORD,
// database: process.env.LDB_NAME,
// port: process.env.LDB_PORT,
// ssl: process.env.LDB_SSL,
// timezone: process.env.LDB_TIME_ZONE,
// multipleStatements: true
// });

// con.connect(function(err) {
//     if (err) console.log(err);
//     console.log("Connected!");
// });

module.exports = con;