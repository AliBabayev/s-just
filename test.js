var dotenv = require('dotenv');
dotenv.config();
var path = require('path')
var con = require('./db.js');

var query = `SELECT *,ST_Distance_Sphere(
    point(today_events_view.Lat, today_events_view.Lon),
    point(40.375563,49.814185)
)/1000 as Distance FROM \`just-db\`.today_events_view;`;
var start_latitude = 40.375424
var start_longitude = 49.814185
var stop_latitude = 40.375424
var stop_longitude = 49.811716
con.query(query, function(err, result, fields) {
    if (err) console.log(err);
    console.log(result);
});