const express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var compression = require('compression');
var helmet = require('helmet');
var dotenv = require('dotenv');
var morgan = require('morgan');
var formidable = require('formidable');
const requestIp = require('request-ip');
var jwt = require('jsonwebtoken');
const app = express();

dotenv.config();

//Includes Middlewares and Routers
var verifyToken = require('./middleware/verifyToken.js');
var authenticationRouter = require('./router/authentication');
var eventsRouter = require('./router/events');
var usersRouter = require('./router/users');
var friendsRouter = require('./router/friends');
var commentsRouter = require('./router/comments');
var statusRouter = require('./router/status');
var stoiresRouter = require('./router/stories');

//Configs
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(compression());
app.use(helmet());
morgan.token('user', function(req, res) { try { var user = jwt.verify(req.header('auth-token'), process.env.SECRET_KEY); return user.username; } catch (err) { return "NoAuth"; } });
morgan.token('getIp', function(req, res) { return requestIp.getClientIp(req) });
app.use(morgan('User-:user - IP::getIp DATE::date[iso] -> [:method] :url | RC::status :response-time ms'));
app.use(express.static(__dirname + '/public/pages'));
//Installing Routers

app.use(process.env.APP_URL, authenticationRouter);
app.use(process.env.APP_URL + '/users', verifyToken, usersRouter);
app.use(process.env.APP_URL + '/events', verifyToken, eventsRouter);
app.use(process.env.APP_URL + '/friends', verifyToken, friendsRouter);
app.use(process.env.APP_URL + '/comments', verifyToken, commentsRouter);
app.use(process.env.APP_URL + '/stories', verifyToken, stoiresRouter);
app.use(process.env.APP_URL + '/status', verifyToken, statusRouter);

app.get('/images/profile/:name', function(req, res) {
    var filepath = path.join(__dirname, '/public/uploads/profile') + '/' + req.params.name;
    res.sendFile(filepath);
});

app.get('/images/stories/:name', function(req, res) {
    var filepath = path.join(__dirname, '/public/uploads/stories') + '/' + req.params.name;
    res.sendFile(filepath);
});

app.get('/images/events/:name', function(req, res) {
    var filepath = path.join(__dirname, '/public/uploads/events') + '/' + req.params.name;
    res.sendFile(filepath);
});

app.post(process.env.APP_URL + '/images/upload', async function(req, res) {
    var form = new formidable.IncomingForm();

    form.parse(req);

    form.on('fileBegin', function(name, file) {
        var pathName = Date.now() + file.name.replace(/\s/g, '');
        file.path = __dirname + '/public/uploads/profile/' + pathName;
        file.name = pathName;
    });

    form.on('file', function(name, file) {
        var fullUrl = req.protocol + '://' + req.get('host') + "/images/profile/";
        console.log('Uploaded ' + fullUrl + file.name);
        res.status(201).send(JSON.stringify({
            "result": "success",
            "message": fullUrl + file.name
        }));
    });

});

app.all('*', function(req, res) {
    var ip = requestIp.getClientIp(req);
    res.status(404).render(__dirname + '/public/pages/404.ejs', { ip: ip });
});

var port = process.env.APP_PORT;
app.listen(port, () => console.log("Server Started " + port));