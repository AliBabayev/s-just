function toRadians(degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
function toDegrees(radians) {
    return radians * 180 / Math.PI;
}

module.exports.bearingX = function(startLat, startLng, destLat, destLng) {
    startLat = toRadians(startLat);
    startLng = toRadians(startLng);
    destLat = toRadians(destLat);
    destLng = toRadians(destLng);

    y = Math.sin(destLng - startLng) * Math.cos(destLat);
    x = Math.cos(startLat) * Math.sin(destLat) -
        Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
    brng = Math.atan2(y, x);
    brng = toDegrees(brng);
    return (brng + 360) % 360;
}

module.exports.bearingY = function(startElevation, destElevation, distance) {
    var height = destElevation - startElevation;
    var symbol = height / -height | 0;
    var triangle = height / distance | 0;
    var b = 90 + (Math.tan(triangle) * (symbol));
    return b;
}

console.log(module.exports.bearingY(68, 68, 10));