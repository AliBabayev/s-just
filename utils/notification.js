var admin = require("firebase-admin");

var serviceAccount = require("./../key.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://app-just-com.firebaseio.com"
});

module.exports = function(title, body, hashtag, type, action_id, image, tokens, priority) {
    var payload = {
        data: {
            "title": title,
            "body": body,
            "hashtag": hashtag,
            "type": type,
            "action_id": action_id,
            "image": image
        }
    };
    var options = {
        "priority": priority,
        timeToLive: 60 * 60 * 24
    };
    admin.messaging().sendToDevice(tokens, payload, options)
        .then(function(response) {
            console.log("Notification has sented");
        })
        .catch(function(err) {
            console.log("Notification has some errors:" + err);
        });
}

// module.exports('alibabayev0', "sent you friendship", '', '2', '1', '', 'fuwFNnopPw0:APA91bGeHho4gJTrFkYdZYPsaRbgdIyxmlNTpaLgJnqYwj6kBKdCIpEu9nZkJN6_weCfgYkybObPWoYjt6qYxZLXu3vBgIxzHDHfLhq2z32W5TAklMynyZxxlJ7SdMedjyFmIEOx4iur', "high");